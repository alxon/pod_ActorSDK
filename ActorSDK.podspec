Pod::Spec.new do |s|
  s.name         = "ActorSDK"
  s.version      = "3.0.452"
  s.summary      = "Actor SDK for intergration Actor Messaging to your apps"
  s.homepage     = "https://actor.im/"
  s.license      = 'MIT'
  s.author       = { "Actor LLC" => "steve@actor.im" }
  s.platform     = :ios, "8.0"
  s.requires_arc = true

  s.source       = { :http => 'https://ios.bars-open.ru/ActorSDK/ActorSDK.zip' }

  # Core
  s.dependency 'RegexKitLite'
  s.dependency 'zipzap'
  s.dependency 'J2ObjC-Framework'
  # s.dependency 'ReachabilitySwift'

  # UI
  s.dependency 'VBFPopFlatButton'
  s.dependency 'MBProgressHUD'
  s.dependency 'SZTextView'
  s.dependency 'RSKImageCropper'
  s.dependency 'JDStatusBarNotification'
  s.dependency 'YYImage'
  s.dependency 'YYImage/WebP'
  s.dependency 'YYCategories'
  s.dependency 'YYWebImage'
  s.dependency 'DZNWebViewController'

  s.dependency 'TTTAttributedLabel'
  s.dependency 'M13ProgressSuite'

  # s.source_files = "Frameworks/ActorSDK.framework", "Frameworks/ActorSDK.framework/**/*"
  s.ios.preserve_paths = '**/*'
  # s.ios.vendored_frameworks = 'Frameworks/ActorSDK.framework'
  s.ios.vendored_frameworks = 'ActorSDK.framework'

  #s.xcconfig = {
    # "SWIFT_INCLUDE_PATHS" => "~/apps/_pods_/ActorSDK/Frameworks/",
    # "FRAMEWORK_SEARCH_PATHS" => "~/apps/_pods_/ActorSDK/Frameworks/"
  #}
end
